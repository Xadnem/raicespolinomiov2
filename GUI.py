#!/usr/bin/python3.7
# -*- coding:utf-8 -*-

import cairo
import Polinomio
import sys
import gi

from Polinomio import Polinomio
from Polinomio import PolinomioCopia

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk

import math
from math import log,ceil


class GUI():

    # Atributos de la clase
    incremento = 0.1  # incremento de la X en cada iteracion

    def __init__(self,pol,precision,vbPrincipal,lbMensajes,tbPol,tipo):

        self.tipo = tipo

        # Establecer la hoja de estilo CSS
        screen = Gdk.Screen.get_default()
        proveedor = Gtk.CssProvider()
        proveedor.load_from_path("estilo.css")
        style_context = Gtk.StyleContext()
        style_context.add_provider_for_screen(screen, proveedor, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        # Ancho y alto del area de la pantalla
        ancho, alto = screen.width(), screen.height() * 0.92
        # Atributos del objeto
        self.pol = pol  # Objeto Polinomio para realizar los calculos
        self.precision = precision # Precision con que se calcularan las raices
        self.polmemo = pol # Objeto polinomio original para recordar cuando se vaya reduciendo el polinomio introducido
        self.derivada = Polinomio([])  # Objeto Polinomio que sera la derivada del polinomio
        self.valoresx = {}  # Diccionario para guardar los valores de x e y de cada iteracion
        self.valoresm = []  # Lista para guardar los valores de la pendiente en cada iteracion
        self.raices = []  # Para guardar las raices
        self.factorx = 50  # Pixeles por unidad del valor de la X
        self.puntero = -1 # Indice del elemento clave/valor del diccionario valorex y de la lista m con los que se está operando
        self.pd = False; # Si es true, indica al metodo on_draw que pinte la derivada
        self.desplx = 0  # Desplazamiento en x desde el origen de coordenadas
        self.desply = 0  # Desplazamiento en y desde el origen de coordenadas
        self.escala = 1  # Factor de escala de visualizacion
        self.tangente = False # Será True cuando se dibuje la primera linea tangente
        self.matrizdetransformacion = cairo.Matrix(1, 0, 0, -1, 0, 0)
        self.directa = False
        # Añadir el VBox para las divisiones verticales
        self.vbPrincipal = vbPrincipal
        self.lbMensajes = lbMensajes
        self.tbPol = tbPol
        # HBox para el area del grafico y los valores
        self.hbGraficoValores = Gtk.HBox()
        self.hbGraficoValores.set_visible(True)
        self.vbPrincipal.pack_start(self.hbGraficoValores,False, False, 0)
        #DrawingArea para el grafico
        self.daGrafico = Gtk.DrawingArea()
        self.daGrafico.set_visible(True)
        self.daGrafico.set_size_request(ancho*0.85,alto*0.6)
        self.daGrafico.set_name("daGrafico")
        self.daGrafico.connect("draw", self.on_draw) 
        self.hbGraficoValores.pack_start(self.daGrafico,False,False,30)
        self.anchog, self.altog = self.daGrafico.get_size_request()
        # Imagen bmp y cairo context para dibujar los gráficos
        self.bitmap = cairo.ImageSurface(cairo.FORMAT_ARGB32, int(ancho*0.85), int(alto*0.4))
        self.cr = cairo.Context(self.bitmap)
        # VBox para los valores de x,y,m
        self.vbValores = Gtk.VBox()
        self.vbValores.set_visible(True)
        self.vbValores.set_spacing(0)
        self.vbValores.set_homogeneous(False)
        self.hbGraficoValores.pack_end(self.vbValores,False,False,0)
        # Etiqueta para el rotulo del valor de x
        self.lbVx = Gtk.Label.new("Valor de X:")
        self.lbVx.set_visible(True)
        self.lbVx.set_name("lbVx")
        self.vbValores.pack_start(self.lbVx, False, False, 10)
        # Etiqueta para el valor de x
        self.lbX = Gtk.Label.new(" 1")
        self.lbX.set_visible(True)
        self.lbX.set_name("lbX")
        self.lbX.set_size_request(ancho*0.12, alto*0.03)
        self.vbValores.pack_start(self.lbX, False, False, 0)
        # Etiqueta para el rotulo de Y
        self.lbVY = Gtk.Label.new("Valor de Y")
        self.lbVY.set_visible(True)
        self.lbVY.set_name("lbVY")
        self.lbVY.set_size_request(ancho * 0.12, alto * 0.03)
        self.vbValores.pack_start(self.lbVY, False, False, 10)
        # Etiqueta para el valor de Y
        self.lbY = Gtk.Label.new("0")
        self.lbY.set_visible(True)
        self.lbY.set_name("lbY")
        self.lbY.set_size_request(ancho * 0.12, alto * 0.03)
        self.vbValores.pack_start(self.lbY, False, False, 0)
        # Etiqueta para el rotulo del valor de la pendiente
        self.lbVm = Gtk.Label.new("Valor de la pendiente")
        self.lbVm.set_visible(True)
        self.lbVm.set_name("lbVm")
        self.vbValores.pack_start(self.lbVm, False, False, 10)
        # Etiqueta para el valor de la pendiente
        self.lbM = Gtk.Label.new(" ")
        self.lbM.set_visible(True)
        self.lbM.set_name("lbM")
        self.lbM.set_size_request(ancho*0.12, alto*0.03)
        self.vbValores.pack_start(self.lbM, False, False, 0)
        # Etiqueta para el rotulo del nuevo valor de x
        self.lbVnuevax = Gtk.Label.new("Nuevo valor de X")
        self.lbVnuevax.set_visible(True)
        self.lbVnuevax.set_name("lbVnuevax")
        self.vbValores.pack_start(self.lbVnuevax, False, False, 10)
        # Etiqueta para el nuevo valor de X
        self.lbNuevax = Gtk.Label.new(" 0")
        self.lbNuevax.set_visible(True)
        self.lbNuevax.set_name("lbNuevax")
        self.lbNuevax.set_size_request(ancho * 0.12, alto * 0.03)
        self.vbValores.pack_start(self.lbNuevax, False, False, 0)
        # Etiqueta para el rotulo de los botones de zoom
        self.lbZoom = Gtk.Label.new("Zoom")
        self.lbZoom.set_visible(True)
        self.lbZoom.set_name("lbZoom")
        self.lbZoom.set_size_request(ancho * 0.12, alto * 0.03)
        self.vbValores.pack_start(self.lbZoom, False, False, 0)
        # Hbox para los botones de zoom
        self.hbZoom = Gtk.HBox()
        self.hbZoom.set_visible(True)
        self.vbValores.pack_start(self.hbZoom,False,False,0)
        # Boton para el zoom +
        self.btZoomMas = Gtk.Button.new_with_label("+")
        self.btZoomMas.set_name("btZoomMas")
        self.btZoomMas.connect("clicked",self.on_btZoom_clicked)
        self.btZoomMas.set_size_request(alto/23,alto/23)
        self.btZoomMas.set_visible(True)
        self.hbZoom.pack_start(self.btZoomMas, False, False, 0)
        # Boton para el zoom -
        self.btZoomMenos = Gtk.Button.new_with_label("-")
        self.btZoomMenos.set_name("btZoomMenos")
        self.btZoomMenos.connect("clicked",self.on_btZoom_clicked)
        self.btZoomMenos.set_size_request(alto/23,alto/23)
        self.btZoomMenos.set_visible(True)
        self.hbZoom.pack_start(self.btZoomMenos, False, False, 0)
        # Boton para el desplazamiento arriba
        self.btArriba = Gtk.Button.new_with_label("\u25B4")
        self.btArriba.set_name("btArriba")
        self.btArriba.connect("clicked", self.on_btDesplazamiento_clicked)
        self.btArriba.set_size_request(alto / 23, alto / 23)
        self.btArriba.set_visible(True)
        self.vbValores.pack_start(self.btArriba, False, False, 0)
        # HBox para los botones de desplazamiento a izquierda y derecha
        self.hbDesplazamiento=Gtk.HBox()
        self.hbDesplazamiento.set_visible(True)
        self.vbValores.pack_start(self.hbDesplazamiento,False,False,0)
        # Boton de desplazamiento a la izquierda
        self.btIzquierda = Gtk.Button.new_with_label("\u25C2")
        self.btIzquierda.set_name("btIzquierda")
        self.btIzquierda.connect("clicked", self.on_btDesplazamiento_clicked)
        self.btIzquierda.set_size_request(alto / 23, alto / 23)
        self.btIzquierda.set_visible(True)
        self.hbDesplazamiento.pack_start(self.btIzquierda, False, False, 0)
        # Boton de desplazamiento a la derecha
        self.btDerecha = Gtk.Button.new_with_label("\u25B8")
        self.btDerecha.set_name("btDerecha")
        self.btDerecha.connect("clicked", self.on_btDesplazamiento_clicked)
        self.btDerecha.set_size_request(alto / 23, alto / 23)
        self.btDerecha.set_visible(True)
        self.hbDesplazamiento.pack_start(self.btDerecha, False, False, 0)
        # Boton para el desplazamiento abajo
        self.btAbajo = Gtk.Button.new_with_label("\u25BE")
        self.btAbajo.set_name("btAbajo")
        self.btAbajo.connect("clicked", self.on_btDesplazamiento_clicked)
        self.btAbajo.set_size_request(alto / 23, alto / 23)
        self.btAbajo.set_visible(True)
        self.vbValores.pack_start(self.btAbajo, False, False, 0)
        # Añadir el HBox para el boton de resolucion directa, los botones de iterar y la derivada
        self.hbDerivada = Gtk.HBox()
        self.hbDerivada.set_visible(True)
        self.vbPrincipal.pack_start(self.hbDerivada, False, False, 0)
        self.vbDerivada = Gtk.VBox()
        self.vbDerivada.set_visible(True)
        self.hbDerivada.pack_start(self.vbDerivada, False, False, 30)
        # Añadir la etiqueta del rotulo de la derivada
        self.lbRotuloDerivada = Gtk.Label.new("Derivada del polinomio:")
        self.lbRotuloDerivada.set_visible(True)
        self.lbRotuloDerivada.set_name("lbRotuloDerivada")
        self.vbDerivada.pack_start(self.lbRotuloDerivada, False, False, 0)
        # Añadir la etiqueta de la derivada
        self.lbDerivada = Gtk.Label.new('Derivada:')
        self.lbDerivada.set_visible(True)
        self.lbDerivada.set_name("lbDerivada")
        self.lbDerivada.set_size_request(ancho*0.75, alto*0.05)
        self.lbDerivada.set_xalign(0.02)
        self.vbDerivada.pack_start(self.lbDerivada, False, False, 0)
        # Añadir el VBox para la etiqueta del rotulo de los botones de las iteraciones
        self.vbIteraciones = Gtk.VBox()
        self.vbIteraciones.set_visible(True)
        self.hbDerivada.pack_start(self.vbIteraciones, False, False, 30)
        # Añadir la etiqueta del rotulo de los botones de las iteraciones
        self.lbIteraciones = Gtk.Label.new("Iteraciones:")
        self.lbIteraciones.set_visible(True)
        self.lbIteraciones.set_name("lbIteraciones")
        self.vbIteraciones.pack_start(self.lbIteraciones, False, False, 0)
        # HBox para los botones de iteracion
        self.hbIteraciones = Gtk.HBox()
        self.hbIteraciones.set_visible(True)
        self.vbIteraciones.pack_start(self.hbIteraciones, False, False, 10)
        # Boton de iteracion atras
        self.btIzq = Gtk.Button.new_with_label('\u25C0')
        self.btIzq.set_name("btIzq")
        self.btIzq.connect("clicked", self.on_iteraciones_clicked)
        self.btIzq.set_size_request(alto/23, alto/23)
        self.btIzq.set_visible(True)
        self.hbIteraciones.pack_start(self.btIzq, False, False, 10)
        # Boton de iteracion adelante
        self.btDer = Gtk.Button.new_with_label('\u25B6')
        self.btDer.set_name("btDer")
        self.btDer.connect("clicked", self.on_iteraciones_clicked)
        self.btDer.set_size_request(alto / 23, alto / 23)
        self.btDer.set_visible(True)
        self.hbIteraciones.pack_start(self.btDer, False, False, 10)
        # Boton para la resolucion directa
        self.btDirecta = Gtk.Button.new_with_label("Resolver")
        self.btDirecta.set_name("btDirecta")
        self.btDirecta.connect("clicked", self.on_btDirecta_clicked)
        self.btDirecta.set_size_request(alto / 19, alto / 31)
        self.btDirecta.set_visible(True)
        self.hbIteraciones.pack_end(self.btDirecta, False, False, 0)
        # HBox para el rotulo y la caja de texto de las raices
        self.hbRaices = Gtk.HBox()
        self.hbRaices.set_visible(True)
        self.vbPrincipal.pack_start(self.hbRaices, False, False, 10)
        # Añadir la etiqueta del rotulo de las raices
        self.lbRaices = Gtk.Label.new("Raices:")
        self.lbRaices.set_visible(True)
        self.lbRaices.set_name("lbRaices")
        self.hbRaices.pack_start(self.lbRaices, False, False, 20)
        # Caja de texto para las raices
        self.tbRaices = Gtk.Entry()
        self.tbRaices.set_visible(True)
        self.tbRaices.set_name("tbRaices")
        self.tbRaices.set_size_request(ancho * 0.7, alto * 0.05)
        self.hbRaices.pack_start(self.tbRaices, False, False, 0)
        # Boton para la siguiente raiz
        self.btSiguiente = Gtk.Button.new_with_label('Siguiente Raiz')
        self.btSiguiente.set_name("btSiguiente")
        self.btSiguiente.connect("clicked", self.on_btSiguiente_clicked)
        self.btSiguiente.set_size_request(alto / 19, alto / 23)
        self.btSiguiente.set_visible(True)
        self.hbRaices.pack_start(self.btSiguiente, False, False, 10)
        self.desplazamiento = False
        # Obtener la derivada del polinomio
        self.ObtenerDerivada()

    def on_btZoom_clicked(self, widget):
        """ Manejador de los botones para el zoom + y el zoom -"""

        if widget.get_name() == "btZoomMas":
            self.factorx += 10
        else:
            self.factorx -= 10
        if self.tangente:
            self.pd = True
        self.daGrafico.set_visible(False)
        self.daGrafico.set_visible(True)

    def on_btDesplazamiento_clicked(self,widget):
        # print("Boton: " + widget.get_name() + " Puntero antes: " + str(self.puntero))
        if widget.get_name() == "btArriba":
            self.desply += 10
        elif widget.get_name() == "btAbajo":
            self.desply -= 10
        elif widget.get_name() == "btIzquierda":
            self.desplx -= 10
        elif widget.get_name() == "btDerecha":
            self.desplx += 10
        if self.tangente:
            self.pd = True
        self.daGrafico.set_visible(False)
        self.daGrafico.set_visible(True)

    def on_btDirecta_clicked(self, widget):
        self.directa = True
        while self.pol != None :
            if self.pol.SinVariables():
                break
            self.ObtenerDerivada()
            nuevaraiz = self.pol.ObtenerRaiz(self.valoresx,self.valoresm,self.precision)
            if nuevaraiz != None:
                self.raices.append(nuevaraiz)
                self.MostrarRaiz()
                raiz = self.raices[len(self.raices)-1]
                self.pol = self.pol.Ruffini(raiz,self.precision)
                if self.pol == None:
                    break;
                self.valoresx.clear()
                self.valoresm.clear()
            else:
                break
        self.Finalizar()


    def on_iteraciones_clicked(self, widget):
        self.tangente = True
        if widget.get_name() == 'btDer':
            self.puntero += 1
        else:
            self.puntero -= 1
        self.pd = True # Hace que se pinte la linea tangente
        self.daGrafico.set_visible(False)
        self.daGrafico.set_visible(True)
        if self.puntero == len(self.valoresx)-1:
            self.MostrarRaiz()
            # Mostrar el boton para la raiz siguiente
            self.btSiguiente.set_visible(True)
            # Obtener el polinomio reducido con la raiz obtenida
            # raiz = self.raices[len(self.raices) - 1]
            # reducido = self.pol.Ruffini(raiz,self.precision)
            # print(reducido.AString())
            # input()

    def MostrarRaiz(self):

        """ Muestra la última raiz del polinomio obtenida"""
        self.lbMensajes.set_text("Se ha obtenido una raiz del polinomio.\nPulsar el botón 'Siguiente raiz' para obtener"
                                 "otra raiz del polinomio en caso de que exista.")
        self.btSiguiente.set_visible(True)
        historic = self.tbRaices.get_text()
        raiz = self.raices[len(self.raices) - 1]
        # Obtener la cantidad de decimales que se veran en funcion de la precision
        decimales = ceil(log(1 / self.precision, 10))
        separador = ';' if len(self.raices) > 1 else ''
        self.tbRaices.set_text(historic + separador + ' x =' + str(round(raiz, decimales)))

    def on_btSiguiente_clicked(self, widget):

        """ Reduce el polinomio con la ultima raiz encontrada, llama a on_draw para dibujarlo y a
            ObtenerDerivada para mostrar su derivada. """
        # Ocultar el boton
        self.btSiguiente.set_visible(False)
        # Reducir el polinomio con la ultima raiz obtenida
        raiz = self.raices[len(self.raices) - 1]
        self.pol = self.pol.Ruffini(raiz, self.precision)
        # Resetear las listas
        self.valoresx.clear();
        self.valoresm.clear();
        self.puntero = -1;
        # Si el polinomio reducido no existe, o es un número finalizar la resolución
        if self.pol == None or self.pol.SinVariables():
            self.Finalizar()
            return;
        # Mostrar el polinomio reducido
        self.tbPol.set_text(self.pol.AString())
        self.lbMensajes.set_text(" Rdducimos el polinomio con la última raiz obtenida usando el método de Ruffini.")
        # Dibujar el polinomio reducido
        self.daGrafico.set_visible(False)
        self.daGrafico.set_visible(True)
        # Mostrar la derivada del polinomio reducido
        if self.tipo == "Newton":
            self.derivada = self.ObtenerDerivada()
            if self.derivada is not None:
                self.lbDerivada.set_text(self.derivada.AString())
        elif self.tipo == "Biseccion":
            self.ObtenerDerivada()







    def ObtenerDerivada(self):

        """ Obtiene la derivada del polinomio , la muestra en la etiqueta de la derivada y llama a on_draw"""
        self.derivada = self.pol.Derivada()
        self.hbDerivada.set_visible(True)
        self.hbRaices.set_visible(True)
        self.lbDerivada.set_text(self.derivada.AString())
        # Llenar el diccionario de valores X Y y la lista de pendientes
        raiz = self.pol.ObtenerRaiz(self.valoresx, self.valoresm, self.precision)
        self.raices.append(raiz)
        # print("Linea 371 Raiz en GUI: ", raiz)
        # input()
        if raiz is None:
            self.lbMensajes.set_text("El polinomio no tiene raices")
            if len(self.raices) == 0:
                self.lbRaices.set_visible(False)
                self.tbRaices.set_visible(False)
            else:
                if not self.directa:
                    self.Finalizar()
            self.lbIteraciones.set_visible(False)
            self.btSiguiente.set_visible(False)
            self.btDirecta.set_visible(False)
            self.vbValores.set_visible(False)
            self.hbIteraciones.set_visible(False)
            self.lbDerivada.set_visible(False)
            self.lbRotuloDerivada.set_visible(False)
        else:
            self.lbMensajes.set_text("Mostrando la gráfica del polinomio. Pulsar los botones de iteración adelante o "
                                     "atrás para ver la resolución paso a paso por el método de Newton "
                                     ", o el botón \"Resolver\" para"
                                     " obtener las raices del polinomio de manera directa.")
        # Dibujar el polinomio
        self.on_draw(Gtk.Widget, self.cr)
        if raiz is not None and abs(raiz) > self.precision:
            self.lbMensajes.set_text("Pulsar el botón de iteraciones adelante para ver el resultado de la siguiente iteración del método de Newton, o el botón \"Resolver\" para obtener directamente las raices del polinomio introducido. ")

    def on_draw(self, wid, cr):

        """ Dibuja el grafico del polinomio introducido """
        if self.pol is None:
            return
        # Factores de escalado para las Y en funcion del factor de x
        factory = self.altog/self.anchog*self.factorx
        cr.set_source_rgb(0, 0, 0)
        cr.paint()
        cr.set_source_rgb(250, 250, 250)
        cr.translate(self.anchog/2+self.desplx, self.altog/2+self.desply)
        cr.transform(self.matrizdetransformacion)
        # Pintar los ejes de coordenada
        self.PintarEjes(cr)
        # Pintar las lineas de division
        self.PintarDivisiones(cr, self.factorx, factory)
        # Pintar el grafico
        self.PintarGrafico(cr, self.factorx, factory)
        if self.pd and self.tipo == "Newton":
            self.PintarDerivada(cr)


    def PintarEjes(self, cr):
        """ Dibuja los ejes de coordenadas del gráfico"""
        # Pintar los ejes
        cr.set_line_width(2)
        # Pintar el eje de abcisas ( X )
        cr.move_to(self.anchog/2*-1-self.desplx, 0-self.desply)
        cr.line_to(self.anchog/2-self.desplx, 0-self.desply)
        # Pintar el eje de ordenadas ( Y )
        cr.move_to(0+self.desplx, self.altog*-1+self.desply)
        cr.line_to(0+self.desplx, self.altog+self.desply)
        cr.stroke()

    def PintarDivisiones(self, cr, factorx, factory):
        
        """ Dibuja las lineas de division en el grafico """
        # Pintar las lineas verticales del lado derecho
        x = 0
        cr.set_line_width(0.2)
        cr.move_to(x+self.desplx, self.altog/2+20+self.desply)
        while x < self.anchog -self.desplx:
            cr.line_to(x+self.desplx, self.altog/-2 - 20+self.desply)
            x += 1 * factorx
            cr.move_to(x+self.desplx, self.altog/2+20+self.desply)
        x = 0
        cr.move_to(x+self.desplx, self.desply)
        # Pintar las lineas verticales del lado izquierdo
        while x > -self.anchog -self.desplx:
            cr.line_to(x+self.desplx, self.altog/2*-1 - 20+self.desply)
            x -= 1 * factorx
            cr.move_to(x+self.desplx, self.altog/2+20+self.desply)
        # Pintar las lineas horizontales de la parte superior
        y = 0
        cr.move_to(-self.anchog/2-self.desplx, y-self.desply)
        while y < self.altog/2+(abs(self.desply)*factory) :
            cr.line_to(self.anchog/2-self.desplx, y-self.desply)
            y += 1 * factory
            cr.move_to(-self.anchog/2-self.desplx, y-self.desply)
        # Pintar las lineas horizontales de la parte inferior
        y = 0
        cr.move_to(-self.anchog/2-self.desplx, y-self.desply)
        while y > -self.altog/2-(abs(self.desply)*factory):
            cr.line_to(self.anchog/2-self.desplx, y-self.desply)
            y -= 1 * factory
            cr.move_to(-self.anchog/2-self.desplx, y-self.desply)

        cr.stroke()

    def PintarGrafico(self, cr, factorx, factory):

        """ Dibuja la curva del polinomio """
        x = 0
        cr.set_source_rgb(255, 0, 0)
        cr.set_line_width(3)
        limit = 10000
        while x < self.anchog/2+self.desplx:
            y = self.pol.EvaluarPolinomio(x)
            cy = (y * factory)-self.desply # coordenada y en pantalla
            cx = (x * factorx)+self.desplx   # coordenada x en pantalla
            if cy < -limit+self.desply or cy > self.altog/2 + limit+self.desply:
                break
            cr.line_to(cx, cy)
            x += GUI.incremento
        cr.stroke()
        x = 0
        GUI.incremento *= -1
        while x  > -self.anchog/2+self.desplx:
            y = self.pol.EvaluarPolinomio(x)
            cy = (y * factory) - self.desply  # coordenada y en pantalla
            cx = (x * factorx) + self.desplx  # coordenada x en pantalla
            if cy < -limit or cy > self.altog + limit:
                break
            cr.line_to(cx, cy)
            x += GUI.incremento
        cr.stroke()
        if self.pd:
            if self.tipo == "Newton":
                self.PintarDerivada(cr)





    def PintarDerivada(self,cr):
        """ Dibuja la linea tangente a la gráfica del polinomio en la posicion X que marca el puntero
            muestra los valores de X , Y , m  y X siguiente y las indicaciones en cada iteracion"""
        self.pd = False
        # Obtener una lista con los valores de las claves en el diccionario
        claves = list(self.valoresx.keys());
        # Definir el ancho de linea y el color
        cr.set_line_width(1)
        cr.set_source_rgb(0.73,0.82,0.18)
        # Obtener el factor de escala para los valores de Y
        factory = self.altog / self.anchog * self.factorx
        # Obtener las coordenadas del punto inicial de la tangente
        if self.puntero < len(claves):
            x = claves[self.puntero]
            y = self.valoresx[x]
            # Obtener la cantidad de decimales que se veran en funcion de la precision
            decimales = ceil(log(1/self.precision, 10))
            # Mostrar los valores en las etiquetas
            self.lbX.set_text(str(round(x, decimales)))
            self.lbY.set_text(str(round(y, decimales)))
            self.lbM.set_text(str(round(self.valoresm[self.puntero], decimales)))
            # Obtener las coordenadas del punto inicial de la tangente en pantalla
            xp = self.desplx +  x * self.factorx
            yp = -self.desply + y*factory
            # Incrementar la posicion del puntero para los valores del diccionario
            # Obtener la coordenada X del punto final de la tangente
            if self.puntero < len(claves)-1:
                x2 = claves[self.puntero+1]
                x2p = self.desplx + x2*self.factorx

                # Obtener el vector paralelo a la linea tangente
                vx = xp - x2p
                vy = yp+self.desply
                # Punto de la recta tangente a una distancia de 1000 de xp,yp
                pinix = xp + vx * 1000
                piniy = yp + vy * 1000
                # Punto de la recta tangente a una distancia de -1000 de xp,yp
                pfinx = xp+vx * -1000
                pfiny = yp+vy * -1000
                # Pintar la tangente
                cr.move_to(pinix,piniy)
                cr.line_to(pfinx,pfiny)
                cr.stroke()
                # Mostrar el valor del nuevo valor de x
                self.lbNuevax.set_text(str(round(x2, decimales)))
                # Dibujar la linea y punto iniciales de la tangente
                y2 = self.valoresx[x2]
                y2p = -self.desply + y2 * factory
                #cr.set_dash([1, 5], 2)
                cr.set_dash([2, 5], 2)
                cr.set_source_rgba(0, 1, 0)
                cr.move_to(xp,yp)
                cr.line_to(xp,0-self.desply)
                cr.stroke()
                # Punto
                cr.set_source_rgba(0, 1, 0)
                cr.arc(xp, yp, 5, 0, math.pi * 2)
                cr.fill()
                # Dibujar la linea vertical final de la tangente
                cr.set_source_rgba(0.48, 1, 1)
                cr.move_to(x2p, -self.desply)
                cr.line_to(x2p, y2p)
                cr.stroke()
                # Punto
                cr.arc(x2p, y2p, 5, 0, math.pi * 2)
                cr.fill()

    def Finalizar(self):

        """ Finaliza una vez obtenidas todas las raices del polinomio """
        self.lbMensajes.set_text("Se han hallado todas las raices del polinomio.")
        self.tbPol.set_text(self.polmemo.AString())
        self.pol = self.polmemo
        self.daGrafico.set_visible(False)
        self.daGrafico.set_visible(True)
        self.lbIteraciones.set_visible(False)
        self.btSiguiente.set_visible(False)
        self.btDirecta.set_visible(False)
        self.vbValores.set_visible(False)
        self.hbIteraciones.set_visible(False)
        self.lbDerivada.set_visible(False)
        self.lbRotuloDerivada.set_visible(False)



"""
win = GUI()
win.connect("destroy", Gtk.main_quit)
win.show()
win.maximize()
Gtk.main()
"""