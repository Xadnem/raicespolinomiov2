import cairo
import sys
import gi
from gi.repository import Gtk, Gdk

from Polinomio import StringAPolinomio
from Polinomio import PolinomioCopia

import GUI
from GUI import GUI

import GUIBiseccion
from GUIBiseccion import GUIBiseccion


class RaicesPolinomio(Gtk.Window):
    def __init__(self):
        # Inicializar la clase base
        Gtk.Window.__init__(self, title="Raices Polinomio Xadnem '20")
        # Atributos del objeto
        self.pol = None  # Objeto Polinomio para realizar los calculos
        self.precision = 0.0  # Precision para el calculo
        # Establecer la hoja de estilo CSS
        screen = Gdk.Screen.get_default()
        proveedor = Gtk.CssProvider()
        proveedor.load_from_path("estilo.css")
        style_context = Gtk.StyleContext()
        style_context.add_provider_for_screen(screen, proveedor, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        # Ancho y alto del area de la pantalla
        ancho, alto = screen.width(), screen.height() * 0.92
        self.set_size_request(ancho, alto)
        self.set_resizable(True)
        self.tipo = None
        # Añadir el VBox para las divisiones verticales
        self.vbPrincipal = Gtk.VBox()
        self.vbPrincipal.set_homogeneous(False)
        self.add(self.vbPrincipal)
        self.vbPrincipal.set_visible(True)
        self.vbPrincipal.set_homogeneous(False)
        self.add(self.vbPrincipal)
        self.vbPrincipal.set_visible(True)
        # Añadir el HBox para la etiqueta de los mensajes y el VBox de los botones de navegacion
        self.hbMensajesBotones = Gtk.HBox()
        self.hbMensajesBotones.set_homogeneous(False)
        self.hbMensajesBotones.set_visible(True)
        self.vbPrincipal.pack_start(self.hbMensajesBotones, False, False, 0)
        # Añadir el vBox para los botones de navegacion
        self.vbBotones = Gtk.VBox()
        self.vbBotones.set_visible(True)
        self.vbBotones.set_spacing(0)
        self.vbBotones.set_homogeneous(False)
        self.hbMensajesBotones.pack_end(self.vbBotones, False, False, 0)
        self.hbMensajesBotones.set_name("hbMensajesBotones")
        # Añadir los botones de navegacion
        # Boton Salir
        self.hbbtSalir = Gtk.HBox()
        self.hbbtSalir.set_visible(True)
        self.btSalir = Gtk.Button(label="Salir")
        self.btSalir.set_name("btSalir")
        self.btSalir.connect("clicked", self.on_btSalir_clicked)
        self.btSalir.set_size_request(ancho / 19, alto / 29)
        self.btSalir.set_visible(True)
        self.hbbtSalir.pack_end(self.btSalir, False, False, 10)
        self.vbBotones.pack_start(self.hbbtSalir, False, False, 10)
        # Boton Nuevo
        self.hbbtNuevo = Gtk.HBox()
        self.hbbtNuevo.set_visible(True)
        self.btNuevo = Gtk.Button(label="Nuevo")
        self.btNuevo.set_name("btNuevo")
        self.btNuevo.connect("clicked", self.on_btNuevo_clicked)
        self.btNuevo.set_size_request(ancho / 19, alto / 29)
        self.btNuevo.set_visible(True)
        self.hbbtNuevo.pack_end(self.btNuevo, False, False, 10)
        self.vbBotones.pack_end(self.hbbtNuevo, False, False, 10)
        # Etiqueta para los mensajes
        self.lbMensajes = Gtk.Label.new("Introducir el polinomio.")
        self.lbMensajes.set_line_wrap(True)
        self.lbMensajes.set_xalign(0);
        self.lbMensajes.set_visible(True)
        self.lbMensajes.set_name("lbMensajes")
        self.lbMensajes.set_size_request(ancho - ancho / 13, alto / 13)
        self.hbMensajesBotones.pack_end(self.lbMensajes, False, False, 10)
        # Hbox para el rotulo del polinomio y la precision
        self.hbRotulos = Gtk.HBox();
        self.hbRotulos.set_visible(True)
        self.vbPrincipal.pack_start(self.hbRotulos, False, False, 0)
        # Rotulo del polinomio
        self.lbPol = Gtk.Label.new("Polinomio")
        self.lbPol.set_visible(True)
        self.lbPol.set_name("lbPol")
        self.lbPol.set_size_request(ancho - ancho / 7, alto / 29)
        self.hbRotulos.pack_start(self.lbPol, False, False, 0)
        # Rotulo de la precision
        self.lbPrecision = Gtk.Label.new("Precisión")
        self.lbPrecision.set_visible(False)
        self.lbPrecision.set_name("lbPrecision")
        self.lbPrecision.set_size_request(ancho / 9, alto / 29)
        self.hbRotulos.pack_end(self.lbPrecision, False, False, 20)
        # HBox para la caja del polinomio y la precision
        self.hbPolPrecision = Gtk.HBox();
        self.hbPolPrecision.set_visible(True)
        self.vbPrincipal.pack_start(self.hbPolPrecision, False, False, 0);
        # Caja de texto para introducir el polinomio
        self.tbPol = Gtk.Entry()
        self.tbPol.set_name("tbPol")
        self.tbPol.set_size_request(ancho * 3 / 3.7, alto / 29)
        self.tbPol.set_visible(True)
        self.tbPol.connect("activate", self.on_tbPol_activate)
        self.hbPolPrecision.pack_start(self.tbPol, False, False, 20)
        # Caja de texto para introducir la precision
        self.tbPrecision = Gtk.Entry()
        self.tbPrecision.set_name("tbPrecision")
        self.tbPrecision.set_visible(False)
        self.tbPrecision.set_size_request(ancho / 6, alto / 29)
        self.tbPrecision.connect("activate", self.on_tbPrecision_activate)
        self.hbPolPrecision.pack_end(self.tbPrecision, False, False, 0)
        self.tbPol.grab_focus()
        # RadioButtons para las opciones de resolución
        # RadioButton que no se verá para iniciar el grupo
        self.rbFicticio = Gtk.RadioButton.new(None)
        # RadioButton para el metodo de Newton
        self.rbNewton = Gtk.RadioButton.new_from_widget(self.rbFicticio)
        self.rbNewton.set_name("rbNewton")
        self.rbNewton.set_label("Método de Newton")
        self.rbNewton.set_visible(False)
        self.rbNewton.connect("clicked",self.on_RadioButtons_clicked)
        self.vbPrincipal.pack_start(self.rbNewton,False,False,0)
        # RadioButton para le metodo de Bisección
        self.rbBiseccion = Gtk.RadioButton.new_from_widget(self.rbFicticio)
        self.rbBiseccion.set_name("rbBiseccion")
        self.rbBiseccion.set_label("Método de Biseccion")
        self.rbBiseccion.set_visible(False)
        self.rbBiseccion.connect("clicked", self.on_RadioButtons_clicked)
        self.vbPrincipal.pack_start(self.rbBiseccion, False, False, 0)
        # RadioButton para le metodo de Regula Falsi
        self.rbRegulaFalsi = Gtk.RadioButton.new_from_widget(self.rbFicticio)
        self.rbRegulaFalsi.set_name("rbRegulaFalsi")
        self.rbRegulaFalsi.set_label("Método de Regula Falsi")
        self.rbRegulaFalsi.set_visible(False)
        self.rbRegulaFalsi.connect("clicked", self.on_RadioButtons_clicked)
        self.vbPrincipal.pack_start(self.rbRegulaFalsi, False, False, 0)

    def on_btSalir_clicked(self, widget):
        """ Manejador para el boton Salir """
        sys.exit()

    def on_btNuevo_clicked(self, widget):

        """ Manejador para el boton Nuevo, será reescrito en otras clases """
        widget.set_visible(True)
        self.set_visible(False)
        nuevo = RaicesPolinomio()
        nuevo.show()

    def on_tbPol_activate(self, widget):

        """ Manejador del evento activate de la caja de texto para introducir el polinomio.
            Construye un objeto Polinomio con el contenido de la caja de texto y lo asigna
            al atributo pol de la clase."""
        widget.set_visible(True)
        ps = self.tbPol.get_text()
        self.pol = StringAPolinomio(ps)
        if self.pol is None:
            dialogo = Gtk.MessageDialog(self,0,Gtk.MessageType.INFO,Gtk.ButtonsType.OK,"El polinomio introducido está"
                                                                                       " mal formado. Ejemplo de formato:"
                                                                                       " 3x^3+2X-8")
            dialogo.run()
            # self.tbPol.set_text("Entrada no válida:")
            self.tbPol.grab_focus()
            dialogo.destroy()
        else:
            self.polmemo = PolinomioCopia(self.pol)
            self.ObtenerPrecision()

    def ObtenerPrecision(self):

        """Muestra la caja de texto para introducir la precisión con la que se realizará el
           cálculo """
        self.tbPrecision.set_visible(True)
        self.lbPrecision.set_visible(True)
        self.lbMensajes.set_text("Introducir la precisión de calculo.")
        self.tbPrecision.grab_focus()

    def on_tbPrecision_activate(self, widget):

        """ Si el valor es correcto, asigna el valor introducido en la caja de texto al atributo precision"""
        widget.set_visible(True)
        precisions = self.tbPrecision.get_text()
        precisions.replace(",", ".")
        try:
            self.precision = float(precisions)
            if self.precision >= 1 or self.precision < 0:
                raise ValueError
            # Mostrar los radiobuttons con los posibles métodos de resolucion
            self.MostrarOpciones()
            #self.hbGraficoValores.set_visible(True)
            # Mostrar la derivada del polinomio
            # self.ObtenerDerivada()
        except ValueError:
            self.tbPrecision.grab_focus()
            self.lbMensajes.set_text("Introducir valores entre 0 y 1 ( Ejemplo: 0.001)")

    def MostrarOpciones(self):
        """ Muestra los radiobuttons con los posibles métodos de resolución"""
        self.lbMensajes.set_text("Seleccionar un método de resolución.")
        self.rbNewton.set_visible(True)
        self.rbBiseccion.set_visible(True)
        self.rbRegulaFalsi.set_visible(True)

    def on_RadioButtons_clicked(self,widget):
        """ Inicia la resolución dependiendo del radiobutton que se haya activado """
        # Eliminar los radiobuttons
        self.rbNewton.destroy()
        self.rbFicticio.destroy()
        self.rbBiseccion.destroy()
        self.rbRegulaFalsi.destroy()
        nombre = widget.get_name()
        if nombre == "rbNewton":
            self.tipo = "Newton"
            resolucionNewton = GUI(self.pol,self.precision,self.vbPrincipal,self.lbMensajes,self.tbPol,self.tipo)
        elif nombre == "rbBiseccion":
            self.tipo = "Biseccion"
            resolucionBiseccion = GUIBiseccion(self.pol,self.precision,self.vbPrincipal,self.lbMensajes,self.tbPol,self.tipo)
        elif nombre == "rbRegulaFalsi":
            self.tipo = "Regula"
            self.lbMensajes.set_text("Metodo de Regula Falsi")

win = RaicesPolinomio()
win.connect("destroy", Gtk.main_quit)
win.show()
win.maximize()
Gtk.main()