import GUI
from GUI import GUI

import math

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk


class GUIBiseccion(GUI):
    def __init__(self,pol,precision,vbPrincipal,lbMensajes,tbPol,tipo):
        self.extremos = []  # Para guardar los valores de los extremos
        self.puntero = 0 # Inicializar el puntero de situacion del elemento de las listas de valores que se mostrará
        GUI.__init__(self,pol,precision,vbPrincipal,lbMensajes,tbPol,tipo) # Inicializar la clase base
        # Modificar los rotulos de las cajas del lado izquierdo
        self.lbVx.set_text("Extremo izquierdo.")
        self.lbVY.set_text("Extremo derecho.")
        self.lbVm.set_text("Punto medio.")
        self.lbNuevax.set_visible(False)
        self.lbVnuevax.set_visible(False)


    def ObtenerDerivada(self):
        """ Llena la lista de valores de los extremos de los intervalos y de valores del polinomio en
        los puntos medios hasta que el valor del polinomio en el punto medio es menor a la precision """

        self.lbDerivada.set_visible(False)
        self.lbRotuloDerivada.set_visible(False)
        # Mostrar el hbox con la caja de texto y la etiqueta de las raices
        self.hbRaices.set_visible = True
        # Llenar la lista de extremos y la lista de valores del polinomio en el punto medio de cada extremo
        raiz = self.pol.ObtenerRaizBiseccion(self.extremos,self.valoresm,self.precision)
        self.raices.append(raiz)
        if raiz is None:
            self.lbMensajes.set_text("El polinomio no tiene raices")
            if len(self.raices) == 0:
                self.lbRaices.set_visible(False)
                self.tbRaices.set_visible(False)
            else:
                if not self.directa:
                    self.Finalizar()
            self.lbIteraciones.set_visible(False)
            self.btSiguiente.set_visible(False)
            self.btDirecta.set_visible(False)
            self.vbValores.set_visible(False)
            self.hbIteraciones.set_visible(False)
        # Dibujar el polinomio
        self.on_draw(Gtk.Widget,self.cr)
        if raiz is not None and abs(raiz) > self.precision:
            self.lbMensajes.set_text(
                "Pulsar el botón de iteraciones adelante para ver el resultado de la siguiente iteración del método de bisección, o el botón \"Resolver\" para obtener directamente las raices del polinomio introducido. ")



    def on_iteraciones_clicked(self, widget):
        """ Dibuja los extremos del intervalo, el punto medio y el valor del polinomio en el punto medio
        en función de la posicion del puntero en las listas de valores del intervalo y de valores del
        punto medio obtenidas en ObtenerDerivada """

        if widget.get_name() == "btDer":
            self.daGrafico.set_visible(False)
            self.daGrafico.set_visible(True)
            self.puntero += 2
        else:
            self.daGrafico.set_visible(False)
            self.daGrafico.set_visible(True)
            self.puntero -= 2


    def on_draw(self, wid, cr):
        GUI.on_draw(self,wid,cr)
        self.PintarIntervalo(cr)


    def PintarIntervalo(self, cr):
        if self.directa:
            return
        """ Dibuja el intervalo en funcion de la posicion del puntero """
        if self.puntero < 0:
            self.puntero = 0
        factory = self.altog/self.anchog*self.factorx
        # Obtener los valores de los extremos y el punto medio del intervalo y los valores del polinomio
        # en esos puntos
        try:
            a = self.extremos[self.puntero]
            b = self.extremos[self.puntero + 1]
        except IndexError:
            return
        fa = self.pol.EvaluarPolinomio(a)
        fb = self.pol.EvaluarPolinomio(b)
        medio = (a + b) / 2
        fmedio = self.pol.EvaluarPolinomio(medio)
        # Mostrar los valores en las etiquetas del lado izquierdo
        valorizquierdo = "x: " + str(round(a,3)) + "     𝒇x: " + str(round(fa,3))
        valorderecho = "x: " + str(round(b,3)) + "     𝒇x: " + str(round(fb,3))
        puntomedio = "x: " + str(round(medio,3)) + "     𝒇x: " + str(round(fmedio,3))
        self.lbX.set_text(valorizquierdo)
        self.lbY.set_text(valorderecho)
        self.lbM.set_text(puntomedio)

        if self.puntero == 0:
            self.btIzq.set_visible(True)
            self.btDer.set_visible(True)
            self.lbIteraciones.set_visible(True)
            self.btDirecta.set_visible(True)
            lectura = "Comenzamos estableciendo un intervalo entre dos puntos, punto de color rojo claro y " \
                      "de color azul, teniendo en cuenta que" \
                      " el valor del polinomio en los extremos del intervalo tenga signo distinto." \
                      "Así aseguramos la existencia de un punto dentro del intervalo donde el valor" \
                      "del polinomio sea cero.\nEn cada iteración se calcula el punto medio entre los extremos" \
                      ", punto verde, y el valor del polinomio en ese punto. Cuando el valor del " \
                      "polinomio en el punto medio sea menor que la precisión introducida, se habrá obtenido una " \
                      "raiz del polinomio."
        else:
            lectura =""
        if self.puntero == len(self.extremos) - 2:
            #lectura = "Se ha obtenido una raiz del polinomio.\nPulsar el botón 'siguiente raiz' para obtener más raices si existen"
            #anterior = self.tbRaices.get_text()
            #self.tbRaices.set_text(anterior + " ; "+str(round(medio,5)))
            self.raices.append(medio)
            self.MostrarRaiz()
            self.extremos.clear()
            self.btDer.set_visible(False)
            self.btIzq.set_visible(False)
            self.lbIteraciones.set_visible(False)
            self.btDirecta.set_visible(False)
            return
        elif fa * fmedio < 0:
            lectura += "\nComo el valor del polinomio en el extremo izquierdo multiplicado por el valor" \
                        "del polinomio en el punto medio es negativo, el extremo izquierdo se mantendrá" \
                        " en la siguiente iteración. El punto medio pasará a ser el extremo derecho."
        elif fb * fmedio < 0:
            lectura += "\nComo el valor del polinomio en el extremo derecho multiplicado por el valor " \
                        "del polinomio en el punto medio es negativo, el extremo derecho se mantendrá " \
                        "en la siguiente iteración. El punto medio pasará a ser el extremo izquierdo."


        # Dibujar el extremo izquierdo
        cr.set_source_rgb(1, 0.49, 0.31)
        cr.arc((a * self.factorx) + self.desplx, 0 - self.desply, 6, 0, math.pi * 2)
        cr.fill()
        # Dibujar el valor del polinomio en el extremo izquierdo
        cr.arc((a * self.factorx)+self.desplx,(fa*factory)-self.desply,6,0,math.pi*2)
        cr.fill()
        # Dibujar la linea entre el extremo izquierdo y el valor del polinomio en ese punto
        cr.set_line_width(2)
        cr.set_dash([2, 5], 2)
        cr.move_to((a*self.factorx)+self.desplx,0-self.desply)
        cr.line_to((a*self.factorx)+self.desplx,(fa*factory)-self.desply)
        cr.stroke()
        # Dibujar el extremo derecho
        cr.set_source_rgb(0.39,0.58,0.92)
        cr.arc((b*self.factorx)+self.desplx,0-self.desply,6,0,math.pi*2)
        cr.fill()
        # Dibujar el valor del polinomio en el extremo derecho
        cr.arc((b * self.factorx) + self.desplx, (fb * factory) - self.desply, 6, 0, math.pi * 2)
        cr.fill()
        # Dibujar la linea entre el extremo derecho y el valor del polinomio en ese punto
        cr.move_to((b*self.factorx)+self.desplx,0-self.desply)
        cr.line_to((b*self.factorx)+self.desplx,(fb*factory)-self.desply)
        cr.stroke()
        # Dibujar el punto medio del intervalo
        cr.set_source_rgb(0, 1, 0)
        cr.arc( (medio * self.factorx)  + self.desplx, 0 - self.desply, 6, 0, math.pi * 2)
        cr.fill()
        # Dibujar valor del polinomio en el punto medio del intervalo
        cr.arc( (medio * self.factorx)  + self.desplx, (fmedio*factory) - self.desply, 6, 0, math.pi * 2)
        cr.fill()
        # Dibujar la linea entre el punto medio y el valor del polinomio en ese punto
        cr.move_to((medio*self.factorx)+self.desplx,0-self.desply)
        cr.line_to((medio*self.factorx)+self.desplx,(fmedio*factory)-self.desply)
        cr.stroke()
        # Mostrar la lectura en la etiqueta de los mensajes
        self.lbMensajes.set_text(lectura)