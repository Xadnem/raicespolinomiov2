#!/usr/bin/python2
# -*- coding:utf-8 -*-

import Termino
from Termino import Termino
from Termino import StringATermino
from Termino import TerminoCopia


""" +++++++++++++++++++++++++++++++++++++++++++++++++++++
+                                                       +
+   CLASE POLINOMIO INICIADA EL 27-4-2020               +
+   ESCRITA POR XADNEM CON AYUDA DE DIOS                +
+                                                       +
++++++++++++++++++++++++++++++++++++++++++++++++++++++"""


class Polinomio:
    def __init__(self, listaterminos):
        self.listaterms = listaterminos

    def AString(self):
        
        """ Devuelve un string correspondiente al objeto Polinomio """
        contador = 0
        string = ""
        for t in self.listaterms:
            if contador > 0 and contador < len(self.listaterms) and t.coeficiente > 0:
                string += '+'
            string += t.AString()
            contador += 1
        return string

    def Derivada(self,):

        """ Devuelve la derivada del polinomio """
        derivados = []
        for t in self.listaterms:
            d = t.Derivada()
            if d.coeficiente != 0:
                derivados.append(d)
        return Polinomio(derivados)

    def EvaluarPolinomio(self,valorx):

        """ Evalua el polinomio de variable unica con el valor de la variable pasado como argumento """
        valor = 0
        # Obtener la variable que se usa en el polinomio
       # print(len(self.listaterms))
        #input()
        # var = self.listaterms[0].variables[0]
        for t in self.listaterms:
            valor += t.EvaluarTermino(valorx)
        return valor

    def ObtenerIndependiente(self):

        """ Devuelve el termino independiente del polinomio """
        independiente = 0
        for t in self.listaterms:
            if t.Esnumero():
                independiente = t.coeficiente
        return independiente

    def ObtenerRaiz(self,diccionario,lista,precision):

        """ Calcula una raiz del polinomio por el método de Newton y mete todos los valores de X e Y
            en el diccionario pasado como primer argumento y los valores de las pendientes en la
            lista pasada como segundo argumento"""
        # Asignar valores iniciales
        x = 1
        y = 1
        xant = 0
        derivada = self.Derivada()
        contador = 0
        # Añadir el valor inicial 1
        # Iterar hasta obtener la raiz o se hayan hecho 50000 iteraciones
        while (abs(x - xant) > precision) and (contador < 5000):
        #while abs(y) > precision and contador < 5000:
            m = derivada.EvaluarPolinomio(x)
            lista.append(m)
            # Si la derivada es cero, sumar a x 0.01 y volver a iterar
            if m == 0:
                diccionario[x] = self.EvaluarPolinomio(x)
                xant = x
                x += 0.01
                continue
            y = self.EvaluarPolinomio(x)
            # Construir la ecuacion de la recta
            tx = m
            b = -m*x+y
            # Obtener el punto de interseccion de la recta con el eje de abcisas
            diccionario[x] = y
            xant = x
            x = -b/tx
            # print("Linea 82 x: ",x)
            # input()
            contador += 1
        if contador < 5000:
            return x
        else:
            return None

    def Ruffini(self, raiz, precision):

        """ Devuelve un polinomio cuyo grado a sido reducido por el método de Ruffini usando la raiz pasada como
         argumento"""
        # Ordenar el polinomio
        self.OrdenarPolinomio()
        # print(self.AString())
        # print(str(raiz))
        # input()
        # Obtener el termino independiente si existe
        independiente = self.ObtenerIndependiente()
        # Obtener una lista de terminos sin el termino independiente
        listat = []
        for t in self.listaterms:
            if not t.Esnumero():
                listat.append(t)
        # Obtener una lista con los coeficientes de los términos añadiendo ceros en los términos que falten
        listacompleta = []
        for t in listat:
            if len(listacompleta) == 0 or t.exponentes[0] == ((listacompleta[len(listacompleta)-1]).exponentes[0])-1:
                listacompleta.append(t)
            else:
                exp = (listacompleta[len(listacompleta)-1]).exponentes[0]
                while exp > (t.exponentes[0])+1:
                    nulo = Termino(0.0, 'x', [exp-1])
                    listacompleta.append(nulo)
                    exp = (listacompleta[len(listacompleta) - 1]).exponentes[0]
                listacompleta.append(t)

        # Si faltan terminos al final añadir ceros
        exp = (listacompleta[len(listacompleta) - 1]).exponentes[0]
        while exp > 1:
            nulo = Termino(0.0, 'x', [exp - 1])
            listacompleta.append(nulo)
            exp = (listacompleta[len(listacompleta) - 1]).exponentes[0]
        # Añadir el termino independiente
        listacompleta.append(Termino(independiente, ' ', [1]))
        # for x in listacompleta:
            # print(x.AString())
        # input()
        # Realizar el proceso de multiplicación y suma de cada coeficiente en la lista y añadir los resultados a una nueva lista
        listafinal = []
        for t in listacompleta:
            if len(listafinal) == 0:
                listafinal.append(t.coeficiente)
            else:
                terminosuma = listafinal[len(listafinal)-1] * raiz
                listafinal.append(t.coeficiente+terminosuma)
        # for x in listafinal:
            # print(str(x))
        # input()
        # Construir el polinomio reducido y devolverlo
        expinicial = self.listaterms[0].exponentes[0]
        listareducido = [ Termino(c, 'x', [expinicial]) for c in listafinal ]
        for i in range (len(listareducido)):
            try:
                listareducido[i].exponentes[0] -= i+1
                if abs(listareducido[i].coeficiente) < precision:
                    listareducido[i].coeficiente = 0.0
            except IndexError:
                break
        # Eliminar la variable del ultimo termino
        (listareducido[len(listareducido)-1]).variables = ' '
        # Eliminar posibles exponentes negativos
        for t in listareducido:
            if t.exponentes[0] < 0:
                t.exponentes[0] = 0
        # Devolver el polinomio reducido
        # print((Polinomio(listareducido)).AString())
        # input()
        if len(listareducido) == 1: # En caso de que sea un polinomio de grado 1 ( no se puede reducir )
            return None
        else:
            return Polinomio(listareducido)






    def OrdenarPolinomio(self):

        """ Ordena los términos del polinomio según su exponente"""
        ordenada = sorted(self.listaterms, key = lambda termino: termino.exponentes[0], reverse = True)
        self.listaterms = ordenada


    def SinVariables(self):

        """ Devuelve True si el polinomio solo contiene numeros sin variables y False en caso contrario """
        sinvariables = True
        for x in self.listaterms:
            if not x.Esnumero():
                sinvariables = False
        return sinvariables

    def ObtenerRaizBiseccion(self,extremos,lista,precision):

        """ LLena el diccionario con los valores izquierdo ( valor ) y derecho ( clave ) de los extremos
        de cada intervalo y la lista con el valor de la funcion de X del punto medio """
        # Comenzar por el intervalo -10000, 10000
        a = -10
        b = 10
        fa = self.EvaluarPolinomio(a)
        fb = self.EvaluarPolinomio(b)
        # Mientras el valor de la funcion de a no tenga signo opuesto a la de la funcion de b y, el
        # valor de a sea menor que el de b , reducir b en 0.001
        # signodistinto = ((fa < 0 and fb > 0) or ( fb < 0 and fa > 0 ))
        signodistinto = (fa < 0 and fb > 0) or ( fa > 0 and fb < 0 )
        while not signodistinto and a != b:
            b -= 0.1
            fb = self.EvaluarPolinomio(b)
            # signodistinto = ((fa < 0 and fb > 0) or ( fb < 0 and fa > 0 ))
            signodistinto = (fa < 0 and fb > 0) or ( fa > 0 and fb < 0 )
        # Si el valor de b alcanza -10000 es que el polinomio no tiene raices
        if a == b:
            return None
        # Calcular la funcion del valor medio del intervalo e ir reduciendo el intervalo mientras el valor
        # del polinomio en el punto medio del intervalo sea mayor a la precision
        pm = (a+b)/2
        extremos.append(a)
        extremos.append(b)
        lista.append(self.EvaluarPolinomio(pm))
        # Mientras el valor del polinomio en el punto medio sea mayor que la precisión, seguir el algoritmo
        while abs(lista[len(lista)-1]) > precision:
            fa = self.EvaluarPolinomio(a)
            fb = self.EvaluarPolinomio(b)
            fm = self.EvaluarPolinomio(pm)
            if fa*fm < 0:
                extremos.append(a)
            else:
                extremos.append(pm)
                a = pm
            if fb * fm < 0:
                extremos.append(b)
            else:
                extremos.append(pm)
                b = pm
            pm = (a+b)/2
            lista.append(self.EvaluarPolinomio(pm))
        return pm





# +++++++++++++++ METODOS ESTATICOS  ++++++++++++++++++++++++++++++
def StringAPolinomio(lectura):
    """Construye un objeto Polinomio en base al string pasado como argumento"""
    # Obtener una lista de terminos en forma de string
    listaterms1 = lectura.split('+')
    listaterms2 = []
    for i, x in enumerate(listaterms1):
        sub = x.split('-')
        for y in sub:
            listaterms2.append(y)
    # Añadir los signos
    i = 0 if lectura[0] == '-' or lectura[0] == '+' else 1
    for c in lectura:
        if c == '-' or c == '+':
            listaterms2[i] = c + listaterms2[i]
            i += 1
    # Convertir la lista de strings en una lista de terminos y devolver el objeto Polinomio
    listaterms = []
    for t in listaterms2:
        try:
            listaterms.append(StringATermino(t))
        except ValueError:
            return None
    pol = Polinomio(listaterms)
    return pol

def PolinomioCopia(original):
    lista = []
    for t in original.listaterms:
        lista.append(TerminoCopia(t))
    return Polinomio(lista)
