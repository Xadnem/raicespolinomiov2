#!/usr/bin/python2
# -*- coding:utf-8 -*-

""" +++++++++++++++++++++++++++++++++++++++++++++++++++++
+                                                       +
+   CLASE TERMINO INICIADA EL 25-4-2020                 +
+   ESCRITA POR XADNEM CON AYUDA DE DIOS                +
+                                                       +
++++++++++++++++++++++++++++++++++++++++++++++++++++++"""


class Termino:
    def __init__(self, coeficiente, variables, exponentes):
        self.coeficiente = coeficiente
        self.variables = variables
        self.exponentes = exponentes



    def AString(self):
        cadena = ''
        """ Devuelve el Termino pasado como argumento en forma de string"""
        if self.coeficiente != 1.0 and self.coeficiente != -1.0:
            if (self.coeficiente).is_integer():
                cadena = str(int(self.coeficiente))
            else:
                cadena = str(self.coeficiente)
        elif self.coeficiente == -1:
            cadena = '-'
        for i in range(len(self.variables)):
            if self.exponentes[i] != 0:
                cadena += self.variables[i]
            if self.exponentes[i] != 1 and self.exponentes[i] != 0:
                cadena += '^'
                if (self.exponentes[i]).is_integer():
                    cadena += str(int(self.exponentes[i]))
                else:
                    cadena += str(self.exponentes[i])
        return cadena

    def Derivada(self):

        """ Devuelve el termino derivado del objeto termino"""
        if self.Esnumero():
            return Termino(0, '\0', [1, ])
        else:
            coeder = self.coeficiente * self.exponentes[0]
            expder = []
            expder.append((self.exponentes[0])-1)
            varder = self.variables
            return Termino(coeder, varder, expder)

    def Esnumero(self):

        """ Devuelve True si el objeto Termino es un numero y False de la contrario"""
        # print("\nTermino: " + self.AString())
        # print("\nLargo: " + str(len(self.variables)))
        # print(str((len(self.variables) == 0) or (self.variables[0] == '\0') or (self.variables[0] == ' ') or (self.variables == " ")))
        # print(self.variables[0])
        # input()
        if (len(self.variables) == 0) or (self.variables[0] == '\0') or (self.variables[0] == ' ') or\
                (self.variables is None) or (self.coeficiente == 0) or (self.exponentes[0] == 0):
            # print("EsNumero")
            return True
        else:
            return False

    def EvaluarTermino(self,valor):

        """ Evalua el termino de variable única con el valor de la variable pasado como argumento """
        if not self.Esnumero():
            valort = valor ** self.exponentes[0]
        else:
            valort = 1
        valort *= self.coeficiente
        return valort

# +++++++++++++++ METODOS ESTATICOS  ++++++++++++++++++++++++++++++


def StringATermino(lectura):

    """ Crea un objeto termino a partir del string pasado como argumento """
    # Instanciacion de variables
    contador = 0
    coefs = ""
    varis = ""
    exponentes = []
    # Descartar los posibles espacios al inicio
    while lectura[contador] == ' ':
        contador += 1
    # Obtener el coeficiente
    while contador < len(lectura) and ParteDeCifra(lectura[contador]):
        coefs += lectura[contador]
        contador += 1
    if len(coefs) == 0 or (len(coefs) == 1 and (coefs[0] == '-' or coefs[0] == '+')):
        coeficiente = 1 if lectura[0] != '-' else -1
    else:
        coeficiente = StringAFloat(coefs)
    # Descartar los posibles espacios
    while contador < len(lectura) and lectura[contador] == ' ':
        contador += 1
    # En caso de que el termino sea un número, construir el termino y devolverlo
    if contador >= len(lectura):
        num = Termino(coeficiente, ' ', [1.0])
        return num
    # Obtener las variables y sus exponentes
    if not (lectura[contador]).isalpha():  # Lanzar excepcion si no hay una variable adecuada
        raise ValueError("Linea 62 de Termino")
    varis += lectura[contador]
    contador += 1
    while contador < len(lectura):
        exps = ''
        if lectura[contador] == '^':  # Si es un signo ^ obtener los caracteres correspondientes al exponente
            contador += 1
            while contador < len(lectura) and ParteDeCifra(lectura[contador]):
                exps += lectura[contador]
                contador += 1
            exp = StringAFloat(exps)
            exponentes.append(exp)
        else:  # Si no es un signo de exponente lanzar una excepcion
            raise ValueError("Linea 122 de Termino")
            exponentes.append(1)
        if contador < len(lectura):
            varis += lectura[contador]
        contador += 1
    # Si el ultimo termino no tiene exponente, añadir exponente 1
    if len(varis) > len(exponentes):
        exponentes.append(1)
    # Construir y devolver el termino
    termino = Termino(coeficiente, varis, exponentes)
    return termino


# Metodos estáticos auxiliares

def ParteDeCifra(caracter):
    """ Devuelve True si el caracter parado como argumento es parte de una cifra o False de lo contrario """

    if caracter in '+-,.0123456789':
        return True
    return False


def StringAFloat(string):
    """Devuelve el float correspondiente al string pasado como argumento"""
    # Cambiar las posibles comas por puntos
    comprobada = string.replace(',', '.')
    try:
        exp = float(comprobada)
        return exp
    except ValueError:
        raise ValueError


def TerminoCopia(original):
    coe = original.coeficiente
    vars = []
    for v in original.variables:
        vars.append(v)
    exps = []
    for f in original.exponentes:
        exps.append(f)
    return Termino(coe, vars, exps)
